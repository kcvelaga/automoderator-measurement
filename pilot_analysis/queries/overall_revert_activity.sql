WITH base AS (
    SELECT
        rev_id,
        DATE(rev_timestamp) AS rev_dt,
        CASE
          WHEN a.actor_name IS NULL THEN 'anon'
          WHEN a.actor_name = '{am_username}' THEN 'automod'
          WHEN NOT (a.actor_name IS NULL AND a.actor_name = '{am_username}') THEN 'user'
          ELSE 'misc'
        END AS user_type
    FROM
        revision r
    JOIN
        actor a
        ON r.rev_actor = a.actor_id
    JOIN
        user u 
        ON a.actor_user = u.user_id
    JOIN
        page p 
        ON r.rev_page = p.page_id
    JOIN
        change_tag ct
        ON r.rev_id = ct.ct_rev_id
    JOIN
        change_tag_def ctd
        ON ct.ct_tag_id = ctd.ctd_id
    WHERE
        ctd_name IN ('mw-undo', 'mw-rollback', 'mw-manual-revert')
        AND DATE(rev_timestamp) BETWEEN DATE('{start_dt}') AND DATE('{end_dt}')
        AND page_namespace = 0
),

grouped AS (
  SELECT 
      rev_dt,
      user_type,
      COUNT(DISTINCT rev_id) AS n_reverts
  FROM 
      base
  GROUP BY
    rev_dt,
    user_type
),

pivot AS (
  SELECT
      rev_dt,
      SUM(CASE WHEN user_type = 'user' THEN n_reverts ELSE 0 END) AS user_reverts,
      SUM(CASE WHEN user_type = 'automod' THEN n_reverts ELSE 0 END) AS automod_reverts,
      SUM(CASE WHEN user_type = 'anon' THEN n_reverts ELSE 0 END) AS anon_reverts
  FROM
      grouped
  GROUP BY
      rev_dt
  ORDER BY
      rev_dt
)

SELECT
  *,
  user_reverts + automod_reverts + anon_reverts AS all_reverts
FROM
  pivot