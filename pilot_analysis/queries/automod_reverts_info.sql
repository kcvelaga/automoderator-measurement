WITH am_reverts AS (
    SELECT

        -- ===========================================================
        -- data related to revert made by Automoderator (prefix: amr_)
        -- ===========================================================
        DISTINCT amr_r.rev_id AS amr_rev_id,
        amr_r.rev_timestamp AS amr_rev_ts,
        DATE(amr_r.rev_timestamp) AS amr_rev_dt,
        MAX(
             CASE
                 WHEN amr_ct_def.ctd_name = 'mw-reverted' THEN TRUE
                 ELSE FALSE
             END
        ) AS amr_is_reverted,
        amr_r.rev_parent_id AS amr_rev_parent_id,
        UNIX_TIMESTAMP(amr_r.rev_timestamp) - UNIX_TIMESTAMP(revr_r.rev_timestamp) AS amr_time_to_revert_secs,

        -- =======================================================================
        -- data related to edit that was reverted by Automoderator (prefix: revr_)
        -- =======================================================================            
        revr_r.rev_id AS revr_rev_id,
        revr_a.actor_id AS revr_actor_id,
        revr_r.rev_timestamp AS revr_rev_ts,
        DATE(revr_r.rev_timestamp) AS revr_rev_dt,
        CASE
            WHEN revr_a.actor_user IS NULL THEN 'anonymous'
            WHEN 
                revr_a.actor_user IS NOT NULL 
                AND (revr_u.user_editcount < 50 
                AND ((UNIX_TIMESTAMP(revr_r.rev_timestamp) - UNIX_TIMESTAMP(revr_u.user_registration)) / (3600*24)) < 30) 
            THEN 'newcomer'
            ELSE 'misc'
        END AS revr_user_type,
        CASE
            WHEN revr_a.actor_user IS NULL THEN NULL
            WHEN revr_a.actor_user IS NOT NULL AND revr_u.user_editcount BETWEEN 1 AND 5 THEN '1-5'
            WHEN revr_a.actor_user IS NOT NULL AND revr_u.user_editcount BETWEEN 6 AND 99 THEN '6-99'
            WHEN revr_a.actor_user IS NOT NULL AND revr_u.user_editcount BETWEEN 100 AND 999 THEN '100-999'
            WHEN revr_a.actor_user IS NOT NULL AND revr_u.user_editcount BETWEEN 1000 AND 4999 THEN '1000-4999'
            WHEN revr_a.actor_user IS NOT NULL AND revr_u.user_editcount > 5000 THEN '5000+'
            ELSE 'misc'
        END AS revr_user_edit_bucket

    FROM
        revision amr_r
    JOIN
        change_tag amr_ct
        ON amr_ct.ct_rev_id = amr_r.rev_id
    JOIN
        change_tag_def amr_ct_def
        ON amr_ct.ct_tag_id = amr_ct_def.ctd_id
    JOIN
        actor amr_a
        ON amr_r.rev_actor = amr_a.actor_id
    JOIN
        revision revr_r
        ON revr_r.rev_id = amr_r.rev_parent_id
    JOIN
        actor revr_a
        ON revr_r.rev_actor = revr_a.actor_id
    LEFT JOIN
        user revr_u
        ON revr_a.actor_user = revr_u.user_id
    WHERE
        amr_a.actor_name = '{am_username}'
        AND DATE(amr_r.rev_timestamp) BETWEEN DATE('{start_dt}') AND DATE('{end_dt}')
    GROUP BY
        amr_r.rev_id
),

reverted_am_reverts AS (
    -- ==============================================================================
    -- data related to reverted Automoderator reverts, if applicable (prefix: revam_)
    -- ==============================================================================
    SELECT
        amr.*,
        revam_r.rev_id AS revam_rev_id,
        revam_r.rev_actor AS revam_actor_id,
        revam_r.rev_timestamp AS revam_rev_ts,
        DATE(revam_r.rev_timestamp) AS revam_rev_dt,
        UNIX_TIMESTAMP(revam_r.rev_timestamp) - UNIX_TIMESTAMP(amr.amr_rev_ts) AS revam_time_to_revert_secs,
        CASE
            WHEN revam_a.actor_user IS NULL THEN 'anonymous'
            WHEN 
                revam_a.actor_user IS NOT NULL 
                AND (revam_u.user_editcount < 50 
                AND ((UNIX_TIMESTAMP(revam_r.rev_timestamp) - UNIX_TIMESTAMP(revam_u.user_registration)) / (3600*24)) < 30) 
            THEN 'newcomer'
            ELSE 'misc'
        END AS revam_user_type,
        CASE
            WHEN revam_a.actor_user IS NULL THEN NULL
            WHEN revam_a.actor_user IS NOT NULL AND revam_u.user_editcount BETWEEN 1 AND 5 THEN '1-5'
            WHEN revam_a.actor_user IS NOT NULL AND revam_u.user_editcount BETWEEN 6 AND 99 THEN '6-99'
            WHEN revam_a.actor_user IS NOT NULL AND revam_u.user_editcount BETWEEN 100 AND 999 THEN '100-999'
            WHEN revam_a.actor_user IS NOT NULL AND revam_u.user_editcount BETWEEN 1000 AND 4999 THEN '1000-4999'
            WHEN revam_a.actor_user IS NOT NULL AND revam_u.user_editcount > 5000 THEN '5000+'
            ELSE 'misc'
        END AS revam_user_edit_bucket
    FROM
        revision revam_r
    JOIN
        am_reverts amr
        ON revam_r.rev_parent_id = amr.amr_rev_id
    JOIN
        actor revam_a
        ON revam_r.rev_actor = revam_a.actor_id
    LEFT JOIN
        user revam_u
        ON revam_a.actor_user = revam_u.user_id
    WHERE
        amr_is_reverted
)

SELECT 
    * ,
    NULL AS revam_rev_id,
    NULL AS revam_actor_id,
    NULL AS revam_rev_ts,
    NULL AS revam_rev_dt,
    NULL AS revam_time_to_revert_secs,
    NULL AS revam_user_type,
    NULL AS revam_user_edit_bucket
FROM 
    am_reverts 
WHERE 
    NOT amr_is_reverted
UNION ALL
SELECT 
    * 
FROM 
    reverted_am_reverts